document.addEventListener('DOMContentLoaded', () => {
  // do your setup here
  console.log('Initialized app');

  require('vibrant');

  var portada = document.querySelector('#portada img');

  var img = document.createElement('img');
  img.setAttribute('src', './portadas/portada_5x.jpg')


  img.addEventListener('load', function() {
    
    var vibrantS = new Vibrant(img);

    var swatches = vibrantS.swatches()

    var fondo = document.querySelector('.singleLinkPage')

    var vibrant = swatches['Vibrant'].getHex()
    var darkvibrant = swatches['DarkMuted'].getHex()


    fondo.style = "background: -moz-linear-gradient(-45deg, " + darkvibrant + " 10%, " + vibrant + " 90%);"
      + "background: -webkit-linear-gradient(-45deg, " + darkvibrant + " 10%," + vibrant + " 90%);"
      + "background: linear-gradient(135deg, " + darkvibrant + " 10%," + vibrant + " 90%);"
      + "filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='" + darkvibrant + "', endColorstr='" + vibrant + "',GradientType=1 );"  


    let line = document.querySelector('.active')
    line.style.background =  swatches['DarkVibrant'].getHex()

    document.querySelector('.linksList').style="--bright:" + swatches['Vibrant'].getHex()

  })

});


window.addEventListener('load', () => {
  //console.log("Se cargo...")
  require('play');
  window.mediaAudio = new MediaPlay("#audioPlay",".playButton","#progress");
  document.querySelector('.singleLinkPage').className = "singleLinkPage";


  require('popbox');
  var popbox = new Popbox({
    blur:true
  })

  let shareButton = document.querySelector('#shareId');
  shareButton.addEventListener('click', function() {

  })



});

//console.log('Start')