window.MediaPlay = class {

    constructor(audioInput, button, progressTag) {
        this.audioInput = audioInput;
        this.button = button;
        //this.process = processCall;
        

        this.audioInputTag = document.querySelector(this.audioInput);
        this.buttonTag = document.querySelector(this.button);
        this.progressTag = document.querySelector(progressTag);
        this.duration = this.audioInputTag.duration;


        this.iconTag = document.getElementById("playId");

        this.buttonTag.addEventListener('click', () => {
         
            if (!this.audioInputTag.paused) {
                this.audioInputTag.pause();
                this.iconTag.setAttribute('xlink:href',"#replay")
            } else {
                this.audioInputTag.play();
                this.iconTag.setAttribute('xlink:href',"#pause")
            }
        });


        this.audioInputTag.addEventListener("timeupdate", (a) => {

            let percentage = this.audioInputTag.currentTime / this.audioInputTag.duration
            let progress = (100 * percentage)
            if (progress > 100) {
                progress = 100
            }
            this.progressTag.style.width = progress + "%"
        });

    }

    //this.convertElapsedTime()
}